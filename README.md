# Basic Guide to NLP
This repository contains a Jupyter notebook with sample python codes for basic to major NLP problems faced when dealing with text.

Following are the topics included:
1. Noise Removal
2. Regex Removal
3. Lexicon Normalization
     * Lemmatization
     * Stemming
4. Object Standardization
5. Syntactical Parsing
     * POS Tagging
6. Entity Parsing
     * Topic Modelling(LDA)
     * Bi-Grams
7. Statistical features
     * TF – IDF
8. Text Classification
     * TextBlob with naive bayes
     * Sklearn with SVM
9. Text Matching
     * Levenshtein Distance
     * Phonetic Matching
     * Cosine Similarity



### Reference
[Ultimate Guide to Understand & Implement Natural Language Processing](https://www.analyticsvidhya.com/blog/2017/01/ultimate-guide-to-understand-implement-natural-language-processing-codes-in-python/)
